[![pipeline status](https://gitlab.com/bufkin_python/startup-automation/badges/master/pipeline.svg)](https://gitlab.com/bufkin_python/startup-automation/-/commits/master)

# Start automation for Windows 10 or Windows 11

This script will update the following programs:

- TeX Live
- Windows 10 or Windows 11
- Chocolatey
- WinGet
