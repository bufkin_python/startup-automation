from logging import info
from os import system
from multiprocessing import Process
from subprocess import Popen
from tkinter.ttk import Style
from distutils.log import info
from logging import Logger, warning
from multiprocessing import Process
from subprocess import Popen
from colorama import Back, Fore, Style



def update_windows():
    """ Searches for Windows updates and installs them a step later. """
    _ = Popen("powershell -command Get-WindowsUpdate").communicate()[0]
    _ = Popen("powershell -command Install-WindowsUpdate").communicate()[0]


def update_chocolatey():
    """ Updated all chocolatey packages. """
    print("Updating chocolatey packages:")
    print(Style.RESET_ALL)
    system('choco upgrade all')


def update_tex_package_manager():
    """ Updates TeX Live. """
    system('tlmgr update -self -all -reinstall-forcibly-removed')
    try:
        print("Updating TeX Live package manager:")
        print(Style.RESET_ALL)
        system('tlmgr update -self -all -reinstall-forcibly-removed')
    except FileNotFoundError:
        print(Fore.RED + "Error: TeX Live is not installed.")


def update_winget():
    """ Updates all programs installed with WinGet."""
    print("Updating WinGet programs:")
    print(Style.RESET_ALL)
    system('winget upgrade --all --include-unknown')


if __name__ == '__main__':
    p1 = Process(target=update_windows)
    p2 = Process(target=update_chocolatey)
    p3 = Process(target=update_tex_package_manager)
    p4 = Process(target=update_winget)
    try:
        p1.start()
        p2.start()
        p3.start()
        p4.start()
        p1.join()
        p2.join()
        p3.join()
        p4.join()
    except RuntimeWarning as e:
        print("Script has been stopped by the user.")
    else:
        print("Script is ready.")
        info("Winget update finished.")
